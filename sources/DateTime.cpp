#include <ctime>
#include <limits>
#include <string>

using namespace std;

class DateTime {
public:
    DateTime();

    string GetCurrentDate();
    string GetExpirationDate(const string& creation, const string& validity);

    ~DateTime();
private:
    uint exprHour{ 0 }, exprDay{ 0 }, exprMonth{ 0 }, exprYear{ 0 };
    string monthDays{ NULL };
    time_t t{ 0 };
    tm ts{ 0 };

    string checkDateLength(const string& date);
    void calculateExpirationDate(const string& validity);
    uint setDateFormat(const uint& day, const uint& month, const uint& year, const uint& hour);
};

DateTime::DateTime() {
    t = time(NULL);
    localtime_r(&t, &ts);
    monthDays = { 
        31, // january
        28, // february
        31, // march
        30, // april
        31, // may
        30, // june
        31, // jule
        31, // august
        30, // september
        31, // october
        30, // november
        31  // december
    };
}

string DateTime::GetExpirationDate(const string& creation, const string& validity) {
    exprYear = stoi(creation.substr(4, 4));
    exprMonth = stoi(creation.substr(2, 2));
    exprDay = stoi(creation.substr(0, 2));
    exprHour = stoi(creation.substr(8, 2));
    
    calculateExpirationDate(validity);

    return checkDateLength(
        to_string(
            setDateFormat(
                exprDay, 
                exprMonth, 
                exprYear,
                exprHour
            )
        )
    );
}

string DateTime::GetCurrentDate() {
    return checkDateLength(
        to_string(
            setDateFormat(
                ts.tm_mday, 
                ts.tm_mon + 1, 
                ts.tm_year + 1900,
                ts.tm_hour
            )
        )
    );
}

void DateTime::calculateExpirationDate(const string& validity) {
    try {
        for (uint _index = 0; _index < stoul(validity); _index++) {
            if (++exprHour > 23) {
                exprHour -= 24;
                if (++exprDay > monthDays[exprMonth - 1]) {
                    exprDay -= monthDays[exprMonth - 1];
                    if (++exprMonth > 12) {
                        exprMonth -= 12;
                        exprYear++;
                    }
                }
            }
        }
    }
    catch (out_of_range& e) {
        exprHour = 0, exprDay = 0, exprMonth = 0, exprYear = 0;
    }
}

string DateTime::checkDateLength(const string& date) {
    return string(10 - date.length(), '0').append(date);
}

uint DateTime::setDateFormat(const uint& day, const uint& month, const uint& year, const uint& hour) {
    return abs((int)(day * 100000000 + month * 1000000 + year * 100 + hour));
}

DateTime::~DateTime() {
    exprHour = exprDay = exprMonth = exprYear = 0;
    monthDays.clear();
    ts = { 0 };
    t = 0;
}