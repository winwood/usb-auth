#include <cstring>
#include <iostream>
#include <libudev.h>

using namespace std;

template<typename T>
class IData {
public:
    virtual void initializeData(T variable) = 0;
    virtual ~IData() {};
};

class User : IData<const char*&> {
public:
    string Serial{ NULL };
    string Name{ NULL };
    string Date{ NULL };
    string Validity{ NULL };

    User(const char*& sysname) { initializeData(sysname); }

    ~User() {
        Name.clear();
        Date.clear();
        Serial.clear();
        Validity.clear();
        *sysname = NULL;
        sysname = nullptr;
    }
private:
    const char** sysname{ nullptr };

    void initializeData(const char*& sysname) { this->sysname = &sysname; }
};

class Device : IData<udev_device*> {
public:
    string Node{ NULL };
    string Serial{ NULL };

    Device(udev_device* device) { initializeData(device); }

    ~Device() {
        Node.clear();
        Serial.clear();
    }
private:
    void initializeData(udev_device* device) {
        if (device) {
            Node = udev_device_get_devnode(device);
            Serial = udev_device_get_property_value(device, "ID_SERIAL_SHORT");
        }
    }
};