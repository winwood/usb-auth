#include <fstream>
#include <iostream>
#include <cryptopp/rsa.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <experimental/filesystem>

using namespace std;
using namespace CryptoPP;
namespace efs = experimental::filesystem;

class IVerifierSchems {
public:
    virtual string GetVerifiedDataFromSignature(char* signature) = 0;
    virtual ~IVerifierSchems() {};
};

class RSASSA_PSSR_SHA256 : IVerifierSchems {
public:
    RSASSA_PSSR_SHA256(int& result);

    string GetVerifiedDataFromSignature(char* signature);

    ~RSASSA_PSSR_SHA256();
private:
    const size_t signatureSize{ 256 };
    string publicFilePath{ NULL };
    RSA::PublicKey publicKey;

    void loadPublicPrimesAndInitializeKey();
};

RSASSA_PSSR_SHA256::RSASSA_PSSR_SHA256(int& result) {
    publicFilePath = "/etc/token/rsa_public";

    if (efs::exists(publicFilePath)) {
        loadPublicPrimesAndInitializeKey();
        result = PAM_SUCCESS;
    }
}

string RSASSA_PSSR_SHA256::GetVerifiedDataFromSignature(char* signature) {
    RSASS<PSSR, SHA256>::Verifier _verifier;
    DecodingResult _dResult{ 0 };
    string _verification{ NULL };
    SecByteBlock _bytes{ 0 };
    
    _verifier = publicKey;
    _bytes = SecByteBlock(_verifier.MaxRecoverableLengthFromSignatureLength(signatureSize));
    _dResult = _verifier.RecoverMessage(_bytes, NULL, 0, (const byte*)signature, signatureSize);

    _bytes.resize(_dResult.messageLength);

    if (_dResult.isValidCoding) {    
        _verification = reinterpret_cast<char*>(_bytes.data());
    }

    return _verification;
}

void RSASSA_PSSR_SHA256::loadPublicPrimesAndInitializeKey() {
    Integer _module_e = 0, _public_e = 0;
    ifstream _publicFile;

    auto GetValueOfPrime = [&] {
        string _row{ NULL };
        getline(_publicFile, _row);
        return Integer(_row.data());
    };

    _publicFile = ifstream(publicFilePath);
    _module_e = GetValueOfPrime();
    _public_e = GetValueOfPrime();

    _publicFile.close();

    publicKey.Initialize(_module_e, _public_e);
}

RSASSA_PSSR_SHA256::~RSASSA_PSSR_SHA256() {
    publicFilePath.clear();
    publicKey = RSA::PublicKey();
}