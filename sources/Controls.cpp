#include <fstream>
#include <experimental/filesystem>
#include "Data.cpp"

using namespace std;
namespace efs = experimental::filesystem;

class UserControl : public User {
public:
    UserControl(const char*& sysname) : User(sysname) {}

    bool CompareUsernames(const char*& sysname) {
        bool result = true;
        for (int _index{ 0}; _index < strlen(sysname); _index++) {
            if (sysname[_index] != Name.at(_index)) {
                result = false;
            }
        }
        return result;
    }

    bool CompareSerials(const string& devSerial) {
        if (Serial.compare(devSerial) == 0) {
            return true;
        }
        return false;
    }
};

class DeviceControl : public Device {
public:
    udev* Handle{ NULL };
    udev_device* Device{ NULL };
    udev_monitor* Monitor{ NULL };
    udev_enumerate* Enumerate{ NULL };
    udev_list_entry* FirstEntry{ NULL };
    udev_list_entry* ListEntry{ NULL };
    
    DeviceControl();

    bool CheckFoundDeviceOnAvailability(vector<::Device*>& devices);
    void SetDeviceMonitorWithUSBDevicesFilter();
    char* LoadSignedDataFromDevice(const string& deviceNode, const int& sector);

    ~DeviceControl();
private:
    const size_t signatureSize{ 256 };
    const size_t sectionsStartPosition{ 256 };
};

DeviceControl::DeviceControl() : ::Device(NULL) {
    Handle = udev_new();

    Enumerate = udev_enumerate_new(Handle);
    udev_enumerate_add_match_subsystem(Enumerate, "block");
    udev_enumerate_scan_devices(Enumerate);

    FirstEntry = udev_enumerate_get_list_entry(Enumerate);
}

bool DeviceControl::CheckFoundDeviceOnAvailability(vector<::Device*>& devices) {
    if (efs::exists(udev_device_get_devnode(Device)) &&
        udev_device_get_parent_with_subsystem_devtype(Device, "usb", "usb_device")) {
            for (::Device*& device : devices) {
                if (device->Serial == udev_device_get_property_value(Device, "ID_SERIAL_SHORT")) {
                    return false;
                }
            }
            return true;
    }
    return false;
}

void DeviceControl::SetDeviceMonitorWithUSBDevicesFilter() {
    Monitor = udev_monitor_new_from_netlink(Handle, "udev");
    udev_monitor_filter_add_match_subsystem_devtype(Monitor, "block", "disk");
    udev_monitor_enable_receiving(Monitor);
}

char* DeviceControl::LoadSignedDataFromDevice(const string& deviceNode, const int& sector)  {
    ifstream _rStream(deviceNode, ios::binary);
    char* _signature = new char[signatureSize + 1]{ 0 };
    _rStream.seekg(sector * sectionsStartPosition);
    _rStream.read(_signature, signatureSize);
    _rStream.close();
    return _signature;
}

DeviceControl::~DeviceControl() {
    if (Handle) udev_unref(Handle);
    if (Enumerate) udev_enumerate_unref(Enumerate);
    if (Monitor) udev_monitor_unref(Monitor);
    if (Device) udev_device_unref(Device);
    if (FirstEntry) FirstEntry = nullptr;
    if (ListEntry) ListEntry = nullptr;
}