#include <unistd.h>
#include <typeinfo>
#include <security/pam_modules.h>
#include "VerifierSchemes.cpp"
#include "Controls.cpp"
#include "DateTime.cpp"

class AuthenticationResult {
public:
    int Value;
    
    AuthenticationResult() { Value = PAM_PERM_DENIED; }
    ~AuthenticationResult() { Value = PAM_PERM_DENIED; }
};

class Authentication {
public:
    Authentication(AuthenticationResult& result);

    int GetVerifiedDevices();
    int LoadUserDataAndCheck(const char*& sysname);
    int GetExpirationDateAndCheckValidity();

    ~Authentication();
private:
    vector<Device*> devices;
    DateTime* date{ nullptr };
    UserControl* user{ nullptr };
    DeviceControl* udev{ nullptr };
    RSASSA_PSSR_SHA256* verifier{ nullptr };
    const uint waitTime{ 10 };

    template<typename T>
    void freeObject(T& object);
};

Authentication::Authentication(AuthenticationResult& result) {
    date = new DateTime();
    verifier = new RSASSA_PSSR_SHA256(result.Value);
}

int Authentication::GetVerifiedDevices() {
    udev = new DeviceControl();

    udev_list_entry_foreach(udev->ListEntry, udev->FirstEntry) {
        const char* _devSysPath{ NULL };

        _devSysPath = udev_list_entry_get_name(udev->ListEntry);
        udev->Device = udev_device_new_from_syspath(udev->Handle, _devSysPath);
        if (udev->CheckFoundDeviceOnAvailability(devices)) {
            devices.push_back(new Device(udev->Device));
        }
    }

    if (devices.empty()) {

        int _fileDescriptor{ 0 }, _counter{ 0 };
        fd_set _fileDescriptors{ 0 };
        timeval _timeout{ waitTime };
        
        udev->SetDeviceMonitorWithUSBDevicesFilter();
        _fileDescriptor = udev_monitor_get_fd(udev->Monitor);

        FD_ZERO(&_fileDescriptors);
        FD_SET(_fileDescriptor, &_fileDescriptors);

        _counter = select(_fileDescriptor + 1, &_fileDescriptors, NULL, NULL, &_timeout);
        if (_counter > 0 && FD_ISSET(_fileDescriptor, &_fileDescriptors)) {
            udev->Device = udev_monitor_receive_device(udev->Monitor);
            if (udev->CheckFoundDeviceOnAvailability(devices)) {
                devices.push_back(new Device(udev->Device));
                return PAM_SUCCESS;
            }
        }
        return PAM_AUTH_ERR;
    }
    return PAM_SUCCESS;
}

int Authentication::LoadUserDataAndCheck(const char*& sysname) {
    user = new UserControl(sysname);
    string _devSerial{ NULL };

    for (int _index = 0; _index < devices.size(); _index++) {
        _devSerial = devices.at(_index)->Serial;

        try {
            user->Serial = verifier->GetVerifiedDataFromSignature(
                udev->LoadSignedDataFromDevice(
                    devices.at(_index)->Node,
                    SERIAL_SECTION
                )
            );

            user->Name = verifier->GetVerifiedDataFromSignature(
                udev->LoadSignedDataFromDevice(
                    devices.at(_index)->Node,
                    USERNAME_SECTION
                )
            );

            user->Date = verifier->GetVerifiedDataFromSignature(
                udev->LoadSignedDataFromDevice(
                    devices.at(_index)->Node,
                    DATETIME_SECTION
                )
            );

            user->Validity = verifier->GetVerifiedDataFromSignature(
                udev->LoadSignedDataFromDevice(
                    devices.at(_index)->Node,
                    VALIDITY_SECTION
                )
            );
        }
        catch (exception& e) {
            cerr << endl << "Exception type: " << typeid(e).name() << endl 
                         << "Description: " << e.what() << endl;
            exit(EXIT_FAILURE);
        }

        try {
            if (user->CompareSerials(_devSerial)) {
                if (user->CompareUsernames(sysname)) {
                    return PAM_SUCCESS;
                }
            }
        }
        catch (exception& e) {
            cerr << endl << "Exception type: " << typeid(e).name() << endl 
                         << "Description: " << e.what() << endl;
            exit(EXIT_FAILURE);
        }
        for (auto& device : devices) {
            freeObject(device);
            devices.pop_back();
        }
    }
    return PAM_PERM_DENIED;
}

int Authentication::GetExpirationDateAndCheckValidity() {
    string _currentDate{ NULL }, _expirationDate { NULL };
    try {
        _currentDate = date->GetCurrentDate();
        _expirationDate = date->GetExpirationDate(user->Date, user->Validity);
    }
    catch (exception& e) {
        cerr << "Exception type: " << typeid(e).name() << endl 
             << "Description: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }

    if (stoi(_currentDate.substr(4, 4)) < stoi(_expirationDate.substr(4, 4))) {
        return PAM_SUCCESS;
    }
    else if (stoi(_currentDate.substr(4, 4)) == stoi(_expirationDate.substr(4, 4))) {
        if (stoi(_currentDate.substr(2, 2)) < stoi(_expirationDate.substr(2, 2))) {
            return PAM_SUCCESS;
        }
        else if (stoi(_currentDate.substr(0, 2)) < stoi(_expirationDate.substr(0, 2))) {
            return PAM_SUCCESS;
        }
        else if (stoi(_currentDate.substr(0, 2)) == stoi(_expirationDate.substr(0, 2))) {
            if (stoi(_currentDate.substr(8, 2)) < stoi(_expirationDate.substr(8, 2))) {
                return PAM_SUCCESS;
            }
        }
    }
    return PAM_ACCT_EXPIRED;
}

template<typename T>
void Authentication::freeObject(T& object) {
    if (object) {
        delete object;
        object = NULL;
    }
}

Authentication::~Authentication() {
    freeObject(date);
    freeObject(user);
    freeObject(udev);
    freeObject(verifier);
    for (auto& device : devices) {
        freeObject(device);
    }
	devices.clear();
}