#include "resources/Enums.hpp"
#include "sources/Authentication.cpp"

#define MAXNAME_SIZE 32

AuthenticationResult Result;
Authentication* Auth = new Authentication(Result);
const char* Sysname = new const char[MAXNAME_SIZE]();

PAM_EXTERN int pam_sm_authenticate(pam_handle_t* pamh, int flags, int argc, const char** argv) {
    Result.Value = pam_get_user(pamh, &Sysname, NULL);
    if (Result.Value == PAM_SUCCESS) {
        Result.Value = Auth->GetVerifiedDevices();
        if (Result.Value == PAM_SUCCESS) {
            Result.Value = Auth->LoadUserDataAndCheck(Sysname);
        }
    }
    
    if (Result.Value != PAM_SUCCESS) {
        delete Auth;
        Auth = nullptr;
    }
    
    return Result.Value;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t* pamh, int flags, int argc, const char** argv) {
    if (Result.Value = PAM_SUCCESS) {
        Result.Value = Auth->GetExpirationDateAndCheckValidity();
    }

    delete Auth;
    Auth = nullptr;

    return Result.Value;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t* pamh, int flags, int argc, const char** argv) {
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t* pamh, int flags, int argc, const char** argv) {
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t* pamh, int flags, int argc, const char** argv) {
    return PAM_SUCCESS;
}