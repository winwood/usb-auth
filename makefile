.PHONY: all clean install
CC = g++
CFLAGS = -fPIC -c
LDFLAGS = -shared
SOURCES = pam_usb.cpp
PREFIX = /lib/x86_64-linux-gnu/security
LIBRARIES = -lpam -ludev -lcryptopp -lstdc++fs
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = pam_usb.so

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $(EXECUTABLE) $(OBJECTS) $(LDFLAGS) $(LIBRARIES)
	
.cpp.o:
	$(CC) $(CFLAGS) $< -o $@ $(LIBRARIES)
	
install: $(EXECUTABLE)
	cp $(EXECUTABLE) $(PREFIX)

clean:
	rm -f $(EXECUTABLE) *.o
