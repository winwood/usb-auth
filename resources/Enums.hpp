#pragma once

enum DeviceSections : int {
    SERIAL_SECTION      =   0,
    USERNAME_SECTION    =   1,
    DATETIME_SECTION    =   2,
    VALIDITY_SECTION    =   3
};